<?php


namespace App\Http\Requests;


class RolesRequest extends Request
{
   public function rules()
   {
       return [
           'name'   =>'sometimes|required|min:1|max:16',
           'status' =>'sometimes|required|integer|between:0,1'
       ];
   }

   public function messages()
   {
       return [
           'name.required'      => '角色名称必填',
           'name.min'           => '角色名称最少1个字符',
           'name.max'           => '角色名称最多16个字符',
           'status.required'    => '状态必选',
           'status.integer'     => '状态必须为整型',
           'status.between'     => '状态的值必须是0或者1',
       ];
   }
}