<?php


namespace App\Http\Requests;


use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     * @author:
     * @date: 2019/5/20 13:45
     */
    public function authorize()
    {
        return true;
    }

    /**
     * 重写错误返回
     * @param Validator $validator
     * @author:
     * @date: 2019/5/20 13:44
     */
    public function failedValidation(Validator $validator)
    {
        $message    = $validator->errors()->all();
        $response   = response()->json([
            'code'      => 10001,
            'message'   => $message[0],
            'data'      =>[]
        ]);
        throw new ValidationException($validator, $response);
    }

    public function rules()
    {
        return [];
    }
}