<?php


namespace App\Http\Controllers\Admin;


class ErrorController extends BaseController
{
    public function error()
    {
        return view('admin.error.have_no_permission');
    }
}