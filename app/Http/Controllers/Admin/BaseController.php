<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\JsonResponse;

class BaseController extends Controller{
    public function __construct()
    {
    }
    use JsonResponse;
}