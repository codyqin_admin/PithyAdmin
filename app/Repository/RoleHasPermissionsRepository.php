<?php


namespace App\Repository;


use App\Models\Admin\RoleHasPermissions;

class RoleHasPermissionsRepository
{
    public static $roleHasPermissions;

    public function __construct(RoleHasPermissions $roleHasPermissions)
    {
        self::$roleHasPermissions = $roleHasPermissions;
    }

    /**
     * 添加角色权限
     * @param $data
     * @return bool
     * @author:
     * @date: 2019/5/24 10:22
     */
    public function add($data)
    {
        return self::$roleHasPermissions::query()->insert($data);
    }

    /**
     * 删除用户权限
     * @param $roleId
     * @return mixed
     * @author:
     * @date: 2019/5/24 10:25
     */
    public function delete($roleId)
    {
        return self::$roleHasPermissions::query()->where('role_id',$roleId)->delete();
    }

    /**
     * 获取用户权限
     * @param string $field
     * @param $where
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/24 10:56
     */
    public function get($field = '',$where)
    {
        return self::$roleHasPermissions::query()->where($where)->pluck($field);
    }


}