<?php


namespace App\Repository;


use App\Models\Admin\Users;

class UsersRepository
{
    protected static $users;

    public function __construct(Users $users)
    {
        self::$users= $users;
    }

    /**
     * 用户添加
     * @param $data
     * @return bool
     * @author:
     * @date: 2019/5/20 15:30
     */
    public function store($data)
    {
        return self::$users::query()->insertGetId($data);
    }

    /**
     * 获取用户信息详情
     * @param $id
     * @return Users[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/20 17:55
     */
    public function get($id)
    {
        return self::$users::find($id);
    }
    /**
     * 用户信息修改
     * @param $data
     * @return mixed
     * @author:
     * @date: 2019/5/20 19:36
     */
    public function update($data)
    {
       $user = self::$users::find($data['id']);
       unset($data['id']);
       foreach ($data as $key=>$val){
           $user->$key = $val;
       }
       return $user->save();
    }

    /**
     * 删除用户
     * @param $id
     * @author:
     * @date: 2019/5/20 20:22
     */
    public function delete($id)
    {
        return self::$users::where('id',$id)->delete();
    }

    /**
     * 用户拉黑
     * @param $id
     * @return bool
     * @author:
     * @date: 2019/5/22 15:32
     */
    public function pullBlack($id,$userStatus)
    {
        $user = self::$users::find($id);
        $user->user_status = $userStatus;
        return $user->save();
    }
}