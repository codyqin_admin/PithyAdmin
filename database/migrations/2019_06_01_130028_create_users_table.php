<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('user_type')->default('1')->comment('用户类型 1  超级管理员  2 普通管理员 3 会员');
            $table->string('name',50)->default('')->comment('用户名');
            $table->string('mobile',11)->default('')->comment('手机号');
            $table->string('email',100)->default('')->comment('邮箱');
            $table->string('nick_name',50)->default('')->comment('昵称');
            $table->tinyInteger('sex')->default('1')->comment('性别  1  男  2  女');
            $table->string('password',100)->comment('密码');
            $table->string('remember_token',255)->default('')->comment('记住token');
            $table->string('last_login_ip',15)->default('')->comment('登录IP');
            $table->integer('last_login_at')->nullable()->comment('最近登录时间');
            $table->tinyInteger('user_status')->default('1')->comment('用户状态 1 正常 0  禁用');
            $table->string('qq_auth',255)->nullable()->comment('qq快捷登陆');
            $table->string('sina_auth',255)->nullable()->comment('新浪快捷登陆');
            $table->string('github_auth',255)->nullable()->comment('github快捷登陆');
            $table->timestamp('created_at')->useCurrent()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
        DB::statement("ALTER TABLE `".DB::getConfig('prefix')."users` comment '用户表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
