<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',50)->comment('菜单名称');
            $table->string('icon',50)->nullable()->comment('菜单图标');
            $table->integer('pid')->comment('父级id');
            $table->string('module',100)->nullable()->comment('模块');
            $table->string('controller',100)->nullable()->comment('控制器');
            $table->string('action',100)->nullable()->comment('方法');
            $table->string('routes',100)->nullable()->comment('路由');
            $table->string('alias',100)->nullable()->comment('路由别名');
            $table->tinyInteger('is_show')->default(1)->comment('是否显示 1 展示 2 隐藏');
            $table->timestamp('created_at')->useCurrent()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
        DB::statement("ALTER TABLE `".DB::getConfig('prefix')."permissions` comment '权限表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
