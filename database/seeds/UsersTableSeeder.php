<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'admin',
            'user_type' => 1,
            'mobile' => '15999999999',
            'email' => 'admin@gmail.com',
            'nick_name' => '超级管理员',
            'sex' => 1,
            'password' => '$2y$10$8IH8n.1Yw6M/BMmisbAdQ.MNdR993KZsvdlV1gtd6ZzIy3h97hUTm',
            'last_login_at' => time(),
            'user_status' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),

        ]);
    }
}
