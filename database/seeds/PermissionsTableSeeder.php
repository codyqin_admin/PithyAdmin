<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'id' => 1,
                'name' => '超级管理员',
                'icon' => 'fa fa-unlock-alt',
                'pid' => '0',
                'module' => 'Admin',
                'controller' => 'IndexController',
                'action' => 'home',
                'routes' => '/admin',
                'alias' => 'admin',
                'is_show' => '1',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 2,
                'name' => '用户管理',
                'icon' => 'fa fa-group',
                'pid' => '1',
                'module' => 'Admin',
                'controller' => 'UsersController',
                'action' => 'index',
                'routes' => '/admin/users',
                'alias' => 'users.index',
                'is_show' => '1',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 3,
                'name' => '添加用户',
                'icon' => '',
                'pid' => '2',
                'module' => 'Admin',
                'controller' => 'UsersController',
                'action' => 'add',
                'routes' => '/admin/users/add',
                'alias' => 'users.add',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 4,
                'name' => '保存添加用户',
                'icon' => '',
                'pid' => '2',
                'module' => 'Admin',
                'controller' => 'UsersController',
                'action' => 'do_add',
                'routes' => '/admin/users/do_add',
                'alias' => 'users.do_add',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 5,
                'name' => '用户编辑',
                'icon' => '',
                'pid' => '2',
                'module' => 'Admin',
                'controller' => 'UsersController',
                'action' => 'edit',
                'routes' => '/admin/users/edit/',
                'alias' => 'users.edit',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 6,
                'name' => '用户编辑保存',
                'icon' => '',
                'pid' => '2',
                'module' => 'Admin',
                'controller' => 'UsersController',
                'action' => 'update',
                'routes' => '/admin/users/update',
                'alias' => 'users.update',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 7,
                'name' => '删除用户',
                'icon' => '',
                'pid' => '2',
                'module' => 'Admin',
                'controller' => 'UsersController',
                'action' => 'delete',
                'routes' => '/admin/users/delete',
                'alias' => 'users.delete',
                'is_show' => '2',
                'created_at' => '2019-05-30 19:42:13'
            ],
            [
                'id' => 8,
                'name' => '拉黑用户',
                'icon' => '',
                'pid' => '2',
                'module' => 'Admin',
                'controller' => 'UsersController',
                'action' => 'pull_back',
                'routes' => '/admin/users/pull_back',
                'alias' => 'users.pull_back',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 9,
                'name' => '角色管理',
                'icon' => 'fa fa-user',
                'pid' => '1',
                'module' => 'Admin',
                'controller' => 'RolesController',
                'action' => 'index',
                'routes' => '/admin/roles',
                'alias' => 'users.roles',
                'is_show' => '1',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 10,
                'name' => '添加角色',
                'icon' => '',
                'pid' => '9',
                'module' => 'Admin',
                'controller' => 'RolesController',
                'action' => 'add',
                'routes' => '/admin/roles/add',
                'alias' => 'roles.add',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 11,
                'name' => '角色添加操作',
                'icon' => '',
                'pid' => '9',
                'module' => 'Admin',
                'controller' => 'RolesController',
                'action' => 'add_roles',
                'routes' => '/admin/roles/add_roles',
                'alias' => 'roles.add_roles',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 12,
                'name' => '角色权限设置',
                'icon' => '',
                'pid' => '9',
                'module' => 'Admin',
                'controller' => 'RolesController',
                'action' => 'permissions',
                'routes' => '/admin/roles/permissions',
                'alias' => 'roles.permissions',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 13,
                'name' => '角色权限设置操作',
                'icon' => '',
                'pid' => '9',
                'module' => 'Admin',
                'controller' => 'RolesController',
                'action' => 'permissions_add',
                'routes' => '/admin/roles/permissions_add',
                'alias' => 'roles.permissions_add',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 14,
                'name' => '角色编辑',
                'icon' => '',
                'pid' => '9',
                'module' => 'Admin',
                'controller' => 'RolesController',
                'action' => 'edit',
                'routes' => '/admin/roles/edit/',
                'alias' => 'roles.edit',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 15,
                'name' => '角色编辑操作',
                'icon' => '',
                'pid' => '9',
                'module' => 'Admin',
                'controller' => 'RolesController',
                'action' => 'save',
                'routes' => '/admin/roles/save',
                'alias' => 'roles.save',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 16,
                'name' => '角色删除',
                'icon' => '',
                'pid' => '9',
                'module' => 'Admin',
                'controller' => 'RolesController',
                'action' => 'delete',
                'routes' => '/admin/roles/delete',
                'alias' => 'roles.delete',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 17,
                'name' => '菜单管理',
                'icon' => 'fa fa-navicon',
                'pid' => '1',
                'module' => 'Admin',
                'controller' => 'PermissionsController',
                'action' => 'index',
                'routes' => '/admin/permissions/index',
                'alias' => 'permissions.index',
                'is_show' => '1',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 18,
                'name' => '菜单列表',
                'icon' => '',
                'pid' => '17',
                'module' => 'Admin',
                'controller' => 'PermissionsController',
                'action' => 'lists',
                'routes' => '/admin/permissions/lists',
                'alias' => 'permissions.lists',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 19,
                'name' => '菜单添加',
                'icon' => '',
                'pid' => '17',
                'module' => 'Admin',
                'controller' => 'PermissionsController',
                'action' => 'add',
                'routes' => '/admin/permissions/add/',
                'alias' => 'permissions.add',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 20,
                'name' => '菜单编辑',
                'icon' => '',
                'pid' => '17',
                'module' => 'Admin',
                'controller' => 'PermissionsController',
                'action' => 'edit',
                'routes' => '/admin/permissions/edit/',
                'alias' => 'permissions.edit',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 21,
                'name' => '菜单保存',
                'icon' => '',
                'pid' => '17',
                'module' => 'Admin',
                'controller' => 'PermissionsController',
                'action' => 'save',
                'routes' => '/admin/permissions/save',
                'alias' => 'permissions.save',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 22,
                'name' => '菜单删除',
                'icon' => '',
                'pid' => '17',
                'module' => 'Admin',
                'controller' => 'PermissionsController',
                'action' => 'delete',
                'routes' => '/admin/permissions/delete',
                'alias' => 'permissions.delete',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 23,
                'name' => '菜单添加操作',
                'icon' => '',
                'pid' => '17',
                'module' => 'Admin',
                'controller' => 'PermissionsController',
                'action' => 'add_permissions',
                'routes' => '/admin/permission/add_permissions',
                'alias' => 'permissions.add_permissions',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 24,
                'name' => '角色权限列表',
                'icon' => '',
                'pid' => '9',
                'module' => 'Admin',
                'controller' => 'RolesController',
                'action' => 'getPermissions',
                'routes' => '/admin/roles/getPermissions',
                'alias' => 'roles.getPermissions',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'id' => 25,
                'name' => '角色权限列表',
                'icon' => '',
                'pid' => '9',
                'module' => 'Admin',
                'controller' => 'UsersController',
                'action' => 'index',
                'routes' => '/admin/users',
                'alias' => 'users.index',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")

            ],
            [
                'id' => 26,
                'name' => '角色权限列表',
                'icon' => '',
                'pid' => '9',
                'module' => 'Admin',
                'controller'=> 'RolesController',
                'action' => 'index',
                'routes' => '/admin/roles',
                'alias' => 'users.roles',
                'is_show' => '2',
                'created_at' => date("Y-m-d H:i:s")

            ]
        ]);
    }
}
