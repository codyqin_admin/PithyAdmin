<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'id' => 1,
            'name' => '超级管理员',
            'created_at' => '2019-04-29 14:02:31',
            'updated_at' => '2019-04-29 14:02:31',
            'status' => 1,
            'descriptions' => '超级管理员'
        ]);
    }
}
