# PithyAdmin

#### 介绍
基于Laravel框架的管理后台

#### 软件架构
- PHP >= 7.0.0
- PHP OpenSSL 扩展
- PHP PDO 扩展
- PHP Mbstring 扩展
- PHP Tokenizer 扩展
- PHP XML 扩展
#### 安装配置
- git clone https://gitee.com/zhangfayuan/PithyAdmin.git
- curl -sS http://install.phpcomposer.com/installer | php
- composer config -g repo.packagist composer https://packagist.laravel-china.org
- composer update
- 修改根目录下 .env.example 文件为 .env
- .env 配置数据库信息
- php artisan migrate
- php artisan db:seed
- 账号：admin
- 密码: admin123
#### 使用说明
[测试地址](http://pithyadmin.zhangfayuan.cn/ "PithyAdmin管理后台")
- 账号：testadmin
- 密码: test123456