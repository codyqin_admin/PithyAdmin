@include('admin.layouts._public_header')
<link rel="stylesheet" href="{{URL::asset('css/bootstrap-table.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('css/jquery.treegrid.min.css')}}">
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>菜单管理</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-3">
                            <a href="{{route('permissions.add')}}">
                                <button class="btn btn-primary" type="button"><i class="fa fa-plus"></i> 新增菜单</button>
                            </a>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script src="{{URL::asset('js/bootstrap-table.min.js')}}"></script>
<script src="{{URL::asset('js/bootstrap-table-treegrid.js')}}"></script>
<script src="{{URL::asset('js/jquery.treegrid.min.js')}}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var $table = $('.table');
    $(function() {
        //控制台输出一下数据
        let data = JSON.parse('{!! $data !!}');
        $table.bootstrapTable({
            data:data,
            idField: 'id',
            dataType:'jsonp',
            columns: [
                { field: 'id',          title: 'ID' },
                { field: 'name',        title: '名称' },
                { field: 'routes',      title: '路由'},
                { field: 'alias',       title: '别名'},
                { field: 'is_show',     title: '是否显示', sortable: false,  align: 'center', formatter: 'statusFormatter'  },
                { field: 'created_at',  title: '创建时间',align: 'center'},
                { field: 'operate',     title: '操作', align: 'center', formatter: 'operateFormatter' },
            ],
            treeShowField: 'name',
            parentIdField: 'pid',

            onResetView: function(data) {
                $table.treegrid({
                    initialState: 'collapsed',// 所有节点都折叠
                    initialState: 'expanded',// 所有节点都展开，默认展开
                    treeColumn: 1,
                    onChange: function() {
                        $table.bootstrapTable('resetWidth');
                    }
                });
                $table.treegrid('getRootNodes').treegrid('expand');

            },
            onCheck:function(row){
                var datas = $table.bootstrapTable('getData');
                selectChilds(datas,row,"id","pid",true);
                selectParentChecked(datas,row,"id","pid")
                $table.bootstrapTable('load', datas);
            },

            onUncheck:function(row){
                var datas = $table.bootstrapTable('getData');
                selectChilds(datas,row,"id","pid",false);
                $table.bootstrapTable('load', datas);
            },
        });
    });

    // 格式化按钮
    function operateFormatter(value, row, index) {
        return [
            '<a href="/admin/permissions/add/'+row.id+'" title="创建子菜单"><i class="fa fa-check-square-o" ></i>&nbsp;创建子菜单</button>&nbsp;&nbsp;|&nbsp;&nbsp;',
            '<a href="/admin/permissions/edit/'+row.id+'/pid/'+row.pid+'" title="编辑菜单"><i class="fa fa-edit" ></i>&nbsp;编辑</button>&nbsp;&nbsp;|&nbsp;&nbsp;',
            '<a href="JavaScript:;" onclick="Pdelete('+row.id+')" title="删除菜单"><i class="fa fa-trash-o" ></i>&nbsp;删除</button>',
        ].join('');

    }
    // 格式化状态
    function statusFormatter(value, row, index) {
        if (value === 1) {
            return '<a href="javascript:;" onclick="return false;" title="显示"> <i class="fa fa-check text-navy"></i></a>';
        } else {
            return '<a href="javascript:;" onclick="return false;" title="不显示"><i class="fa fa-close text-danger"></i></a>';
        }
    }

    function Pdelete(id) {
        var lock=false;//默认未锁定
        layer.confirm('确定要删除该菜单吗？', {
            btn: ['确定','取消'] //按钮
        }, function(){
            if(!lock){
                lock = true;
                $.post("{{route('permissions.delete')}}",{id:id},
                    function (data) {
                        if(data.code === 10000){
                            layer.msg('菜单删除'+data.message, {icon: 1,time:1500},function () {
                                window.location.href='{{route("permissions.index")}}'
                            });
                        }else{
                            layer.msg(data.message, {icon: 5,time:2000});
                        }
                    },'json')
            }

        });
    }
</script>
@include('admin.layouts._public_footer')