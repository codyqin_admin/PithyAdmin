<?php
Route::get('/','IndexController@index');
Route::get('/test','IndexController@test');
Route::get('/login','IndexController@test');

Route::namespace('Admin')->group(function () {
    // 在 "App\Http\Controllers\Admin" 命名空间下的控制器
    //中间件  判断有没有登陆
    Route::group(['middleware' => 'check.login'], function() {
        //内部为，不想让未登录用户进的路由
        Route::get('/admin','IndexController@index');
        Route::get('/admin/home','IndexController@home')->name('admin.home');
        Route::group(['middleware' => 'auth.verify'], function() {
            //管理员管理
            Route::match(['get', 'post'], '/admin/users', 'UsersController@index')->name('users.index');
            Route::match(['get', 'post'], '/admin/users/add', 'UsersController@add')->name('users.add');
            Route::post('/admin/users/do_add', 'UsersController@doAdd')->name('users.do_add');
            Route::get('/admin/users/edit/{id}', 'UsersController@edit')->name('users.edit');
            Route::post('/admin/users/update', 'UsersController@update')->name('users.update');
            Route::post('/admin/users/delete', 'UsersController@delete')->name('users.delete');
            Route::post('/admin/users/pull_back', 'UsersController@pullBack')->name('users.pull_back');
            Route::match(['get', 'post'], '/admin/users/edit_admin', 'UsersController@editAdmin')->name('users.edit_admin');
            //角色管理
            Route::match(['get', 'post'], '/admin/roles', 'RolesController@index')->name('users.roles');  //角色列表
            Route::get('/admin/roles/add', 'RolesController@add')->name('roles.add'); //角色添加模板
            Route::post('/admin/roles/add_roles', 'RolesController@addRoles')->name('roles.add_roles'); // 角色添加操作
            //获取角色权限列表
            Route::match(['get', 'post'], '/admin/roles/getPermissions', 'RolesController@getPermissions')->name('roles.getPermissions');
            Route::get('/admin/roles/permissions', 'RolesController@permissions')->name('roles.permissions');//  权限设置列表
            Route::post('/admin/roles/permissions_add', 'RolesController@permissionsAdd')->name('roles.permissions_add'); //权限设置操作
            Route::get('/admin/roles/edit/{id}', 'RolesController@edit')->name('roles.edit'); //权限设置操作
            Route::post('/admin/roles/save', 'RolesController@save')->name('roles.save'); //权限设置操作
            Route::post('/admin/roles/delete', 'RolesController@delete')->name('roles.delete'); //权限设置操作

            //菜单管理
            Route::get('/admin/permissions/index', 'PermissionsController@index')->name('permissions.index');
            Route::get('/admin/permissions/lists', 'PermissionsController@lists')->name('permissions.lists');
            Route::get('/admin/permissions/add/{id?}', 'PermissionsController@add')->name('permissions.add');
            Route::get('/admin/permissions/edit/{id}/pid/{pid}', 'PermissionsController@edit')->name('permissions.edit');
            Route::post('/admin/permissions/save', 'PermissionsController@save')->name('permissions.save');
            Route::post('/admin/permissions/delete', 'PermissionsController@delete')->name('permissions.delete');
            Route::post('/admin/permission/add_permissions', 'PermissionsController@addPermissions')->name('permissions.add_permissions');
        });
    });
    Route::match(['get','post'],'/admin/user/edit_head_portrait','UsersController@editHeadPortrait');
    Route::get('/admin/login','IndexController@login');
    Route::get('/admin/error','ErrorController@error');
    Route::get('/admin/login_out','IndexController@loginOut');
    Route::post('/admin/doLogin','IndexController@doLogin')->name('doLogin');
});
